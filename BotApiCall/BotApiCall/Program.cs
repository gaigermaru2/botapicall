﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BotApiCall
{
    class Program
    {
        static void Main(string[] args)
        {
            string periodFrom = "2020-12";
            string periodTo = "2021-01";

            DateTime dt = DateTime.Now.AddMonths(-1);
            var dx1 = GetLastFridayOfTheMonth(dt);
            periodTo = dx1.ToString("yyyy-MM-dd");
            DateTime dt2= DateTime.Now.AddMonths(-1);
            var dx2 = GetLastFridayOfTheMonth(dt);
            periodFrom = dx2.ToString("yyyy-MM-dd");
            CreateToDB(periodFrom, periodTo,"USD");
            CreateToDB(periodFrom, periodTo, "AUD");
            CreateToDB(periodFrom, periodTo, "JPY");
            CreateToDB(periodFrom, periodTo, "SGD");
            int i = 0;
        }
        private static DateTime GetLastFridayOfTheMonth(DateTime date)
        {
            var lastDayOfMonth = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));

            while (lastDayOfMonth.DayOfWeek != DayOfWeek.Friday)
                lastDayOfMonth = lastDayOfMonth.AddDays(-1);

            return lastDayOfMonth;
        }
        public static void CreateToDB(string periodFrom,string periodTo, string currencyName)
        {
            try
            {
                string restURI = @Properties.Settings.Default.url + "?start_period=" + periodFrom + "&end_period=" + periodTo + "&currency=" + currencyName;
                var client = new RestClient(restURI);
                Console.WriteLine("Call Rest API   currency_code = " + currencyName + "  &&   periodFrom = " + periodFrom + " && periodTo = " + periodTo);
                Console.WriteLine("URI = "+ restURI);
                var request = new RestRequest(Method.GET);
                request.AddHeader("x-ibm-client-id", Properties.Settings.Default.xibmclientid);
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                request.AddHeader("accept", "application/json");
                IRestResponse response = client.Execute(request);
                Console.WriteLine("received Response from BOT...");

                JObject joResponse = JObject.Parse(response.Content);
                JObject ojObject = (JObject)joResponse["result"];
                JArray array = (JArray)ojObject["data"]["data_detail"];


                using (SqlConnection openCon = new SqlConnection(Properties.Settings.Default.conn))
                {
                    string currency = "INSERT into currency_rate_bot (currency_code,currency_name,currency_name_eng,period,buying_sight,buying_transfer,selling,mid_rate)" +
                        " VALUES (@currency_code,@currency_name,@currency_name_eng,@period,@buying_sight,@buying_transfer,@selling,@mid_rate)";



                    SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM currency_rate_bot WHERE currency_code =  '"+ array[0]["currency_id"] + "'  and   period='"+ array[0]["period"] + "'", openCon);
                    DataSet ds = new DataSet();
                    da.Fill(ds);

                    if (ds.Tables[0].Rows.Count <= 0)
                    {



                        using (SqlCommand que = new SqlCommand(currency))
                        {
                            que.Connection = openCon;
                            que.Parameters.Add("@currency_code", System.Data.SqlDbType.VarChar, 50).Value = array[0]["currency_id"];
                            que.Parameters.Add("@currency_name", System.Data.SqlDbType.VarChar, 50).Value = array[0]["currency_name_th"];
                            que.Parameters.Add("@currency_name_eng", System.Data.SqlDbType.VarChar, 50).Value = array[0]["currency_name_eng"];
                            que.Parameters.Add("@period", System.Data.SqlDbType.VarChar, 50).Value = array[0]["period"];

                            SqlParameter buying_sight = new SqlParameter("@buying_sight", SqlDbType.Decimal);
                            buying_sight.SourceColumn = "buying_sight";
                            buying_sight.Precision = 18;
                            buying_sight.Scale = 6;
                            buying_sight.Value = array[0]["buying_sight"].ToString();

                            SqlParameter buying_transfer = new SqlParameter("@buying_transfer", SqlDbType.Decimal);
                            buying_transfer.SourceColumn = "buying_transfer";
                            buying_transfer.Precision = 18;
                            buying_transfer.Scale = 6;
                            buying_transfer.Value = array[0]["buying_transfer"].ToString();

                            SqlParameter selling = new SqlParameter("@selling", SqlDbType.Decimal);
                            selling.SourceColumn = "selling";
                            selling.Precision = 18;
                            selling.Scale = 6;
                            selling.Value = array[0]["selling"].ToString();

                            SqlParameter mid_rate = new SqlParameter("@mid_rate", SqlDbType.Decimal);
                            mid_rate.SourceColumn = "mid_rate";
                            mid_rate.Precision = 18;
                            mid_rate.Scale = 6;
                            mid_rate.Value = array[0]["mid_rate"].ToString();

                            que.Parameters.Add(buying_sight);
                            que.Parameters.Add(buying_transfer);
                            que.Parameters.Add(selling);
                            que.Parameters.Add(mid_rate);
                            //que.Parameters.Add("@buying_sight", System.Data.SqlDbType.Decimal, 6, array[0]["buying_sight"].ToString());
                            //que.Parameters.Add("@buying_transfer", System.Data.SqlDbType.Decimal, 6, array[0]["buying_transfer"].ToString());
                            //que.Parameters.Add("@selling", System.Data.SqlDbType.Decimal, 6, array[0]["selling"].ToString());
                            //que.Parameters.Add("@mid_rate", System.Data.SqlDbType.Decimal, 6, array[0]["mid_rate"].ToString());
                            openCon.Open();
                            que.ExecuteNonQuery();
                        }
                    }
                    else
                    {
                        Console.WriteLine("Already exist   currency_code = "+ array[0]["currency_id"] + "  &&   period = "+ array[0]["period"]);
                    }
                }
            }
            catch ( Exception e)
            {
                Console.WriteLine("Error Connection ");
                Console.WriteLine("Message= "+e.ToString());
                Console.ReadKey();
            }
            
        }
    }
}
